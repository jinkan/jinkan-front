import { FC } from 'react';

import { SampleType } from '@/types';

interface SampleProps extends SampleType {}

const Sample: FC<SampleProps> = ({ sample }) => {
  return (
    <div>
      <div>{sample}</div>
    </div>
  );
};

export default Sample;
