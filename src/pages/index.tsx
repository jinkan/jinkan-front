import type { NextPage } from 'next';
import Head from 'next/head';

import Sample from '@/components/inputs/Sample';

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Jinkan</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>Jinkan</div>
      <div>
        <Sample sample="サンプルコンポーネント" />
      </div>
    </div>
  );
};

export default Home;
